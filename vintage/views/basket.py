from vintage.app import app, db
from vintage.models import Wine, Basket
from flask import render_template, make_response, request, redirect, url_for
from flask.ext.login import login_required, current_user
from datetime import datetime

@app.route("/basket", methods=["POST", "GET"])
@login_required
def basket():
    bask = request.cookies.get(current_user.username+"basket") or ''
    if "addToCart" in request.form and "id" in request.form:
        bask += request.form["id"]+' '

        resp = make_response(render_template("basket.html", wine=[
            Wine.getWine(id) for id in [int(b) for b in bask.split()]]))
        resp.set_cookie(current_user.username+"basket", bask)

        return resp

    return render_template("basket.html", wine=[
            Wine.getWine(id) for id in [int(b) for b in bask.split()]])

@app.route("/command", methods=['POST'])
@login_required
def command():
    bask = request.cookies.get(current_user.username+"basket") or ''
    bask = [int(b) for b in bask.split()]
    time = datetime.now().isoformat(sep=" ").split('.')[0]

    for b in bask:
        db.session.add(Basket(current_user.username, b, time))
    db.session.commit()

    resp = make_response(redirect(url_for("member",
                                          username=current_user.username)))
    resp.set_cookie(current_user.username+"basket", '')
    return resp

@app.route("/basket/details/<int:ind>")
@login_required
def basket_details(ind=1):
    from werkzeug.exceptions import NotFound
    if ind <= 0:
        raise NotFound
    basket = list(current_user.getBaskets().items())[ind-1]
    
    return render_template("basket_details.html",
                           id = ind,
                           date = basket[0],
                           basket=basket[1])

@app.route("/basket/remove/<int:id>")
@login_required
def removeProduct(id):
    bask = request.cookies.get(current_user.username+"basket") or ''
    bask = [int(b) for b in bask.split()]

    bask.remove(id)

    bask = [str(b) for b in bask]
    bask = ' '.join(bask)

    resp = make_response(redirect(url_for("basket")))
    resp.set_cookie(current_user.username+"basket", bask)

    return resp
