from flask import render_template, url_for, redirect

from vintage.views.home   import home
from vintage.views.about  import about
from vintage.views.search import advSearch
from vintage.views.login  import login
from vintage.views.wine   import wine
from vintage.views.member import member
from vintage.views.basket import basket
from vintage.views.edit   import edit, addWine
