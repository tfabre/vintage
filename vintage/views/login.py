from vintage.app import app
from vintage.models import User
from vintage.commands import newuser

from flask import render_template, url_for, redirect, request
from flask.ext.wtf import Form
from flask.ext.login import login_user, current_user, logout_user

from wtforms import StringField, HiddenField, PasswordField
from wtforms.validators import DataRequired
from hashlib import sha256

class LoginForm(Form):
	username = StringField('Username')
	password = PasswordField('Password')
	next = HiddenField()

	def get_authentificated_user(self):
		user = User.query.get(self.username.data)
		if not user:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None

class SignupForm(Form):
	usernamesu = StringField('Username')
	passwordsu = PasswordField('Password')
	repasswordsu = PasswordField('Retype password')
	next = HiddenField()

	def create_user(self):
		if self.passwordsu.data == self.repasswordsu.data and self.usernamesu.data != "" and self.passwordsu.data != "":
                        return newuser(self.usernamesu.data, self.passwordsu.data)
		else:
			return 2

@app.route("/login/", methods=("GET","POST",))
def login():
	if current_user.get_id() == None:
		f = LoginForm()
		signup = SignupForm()
		errors = None
		user = None
		if not f.is_submitted():
			f.next.data = request.args.get("next")
		elif f.validate_on_submit():
			if "login" in request.form:
				user = f.get_authentificated_user()
			elif "signup" in request.form:
				newuser = signup.create_user()
			
				if newuser == 1:
					errors = ["Username already used."]
				elif newuser == 2:
					errors = ["The passwords don't match."]
				else:
					user = newuser
			if user:
				login_user(user)
				next = f.next.data or url_for("home")
				return redirect(next)
			else:
				errors = errors or ["Incorrect username or password."]
		return render_template("login.html", form=f, signup=signup, errors=errors)
	else:
		return redirect(url_for("home"))

@app.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('home'))
