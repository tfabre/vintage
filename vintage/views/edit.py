from vintage.app import app, db
from vintage.models import Wine
from werkzeug import FileStorage, secure_filename
from flask import render_template, request, url_for, redirect
from flask.ext.wtf import Form
from flask.ext.wtf.file import FileAllowed
from flask.ext.login import login_required, current_user
from wtforms import StringField, HiddenField, FileField, DecimalField, SelectField
from wtforms.validators import Optional
from collections import OrderedDict

class WineForm(Form):
    id       = HiddenField("id")
    category = SelectField("Category", choices=[('Sparkling', 'Sparkling'),
                                                ('White', 'White'),
                                                ('Red', 'Red'),
                                                ('Rose', 'Rose'),
                                                ('Dessert', 'Dessert')],
                           validators=[Optional()])
    image    = FileField("Image", validators=[FileAllowed(['jpg', 'jpeg', 'png'], "Image Only"), Optional()])
    country  = StringField("Country", validators=[Optional()])
    quantity = DecimalField("Quantity", places=None, validators=[Optional()])
    region   = StringField("Region", validators=[Optional()])
    varietal = StringField("Varietal", validators=[Optional()])
    name     = StringField("Name", validators=[Optional()])
    vintage  = DecimalField("Vintage", places=None, validators=[Optional()])
    next = HiddenField()

    def has_file(self):
        if not isinstance(self.data, FileStorage):
            return False
        return self.data.filename not in [None, '', '<fdopen>']

    FileField.has_file = has_file

    def fillSelectedWine(self):
        wine = Wine.getWine(int(self.id.data))

        wine.category = self.category.data
        wine.country  = self.country.data
        wine.quantity = int(self.quantity.data) if self.quantity.data else None
        wine.region   = self.region.data
        wine.varietal = self.varietal.data
        wine.name     = self.name.data
        wine.vintage  = int(self.vintage.data) if self.vintage.data else None
        
        return wine
        
    def createWine(self):
        wine = Wine()
        wine.category = self.category.data
        wine.country  = self.country.data
        wine.quantity = int(self.quantity.data) if self.quantity.data else None
        wine.region   = self.region.data
        wine.varietal = self.varietal.data
        wine.name     = self.name.data
        wine.vintage  = int(self.vintage.data) if self.vintage.data else None
        
        return wine

    def validate(self):
        file = self.image.data
        if file.filename != '' and not file.filename.split('.')[-1] in ['jpg', 'jpeg', 'png']:
            return False
        if not Form.validate(self):
            return False
        return True

@app.route("/edit/<int:id>")
@login_required
def edit(id):
    if current_user.status == 'admin':
        bottle = Wine.getWine(id)
        
        form = WineForm(id=bottle.id, country=bottle.country,
                        quantity=bottle.quantity, region=bottle.region,
                        varietal=bottle.varietal, name=bottle.name,
                        vintage=bottle.vintage, category=bottle.category)

        return render_template("edit.html",
                               bottle = bottle,
                               form=form)
    return redirect(url_for('home'))

@app.route("/edit/process/", methods=["GET", "POST"])
@login_required
def processEdit():
    if current_user.status == 'admin':
        f = WineForm()
        next = request.args.get("next") or url_for('edit', id=int(f.id.data))
        
        if not f.is_submitted():
            f.next.data = request.args.get("next")

        elif request.method == "POST" and f.validate():
            wine = f.fillSelectedWine()
            print(f.image.data)
            if f.image.data and f.image.data.filename != '':
                filename = secure_filename(f.image.data.filename)
                
                import os
                f.image.data.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                wine.image = filename
            
            db.session.commit()
        
            return redirect(next)

    return redirect(url_for('home'))

@app.route("/add/")
@login_required
def addWine():
    if current_user.status == 'admin':
        bottle = Wine()
        
        form = WineForm(id=bottle.id, country=bottle.country,
                        quantity=bottle.quantity, region=bottle.region,
                        varietal=bottle.varietal, name=bottle.name,
                        vintage=bottle.vintage, category=bottle.category)

        return render_template("addWine.html",
                               bottle = bottle,
                               form=form)
    return redirect(url_for('home'))

@app.route("/add/process/", methods=["GET", "POST"])
@login_required
def processAdding():
    if current_user.status == 'admin':
        f = WineForm()
        next = request.args.get("next") or url_for('addWine')
        
        if not f.is_submitted():
            f.next.data = request.args.get("next")

        elif request.method == "POST" and f.validate():
            wine = f.createWine()

            if f.image.data and f.image.data.filename != '':
                filename = secure_filename(f.image.data.filename)
                
                import os
                f.image.data.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                wine.image = filename

            db.session.add(wine)
            db.session.commit()

            next = request.args.get("next") or url_for('wine', id=wine.id)            
            return redirect(next)

    return redirect(url_for('home'))
