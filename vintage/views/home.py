from vintage.app import app
from vintage.models import Wine
from flask import render_template

@app.route("/")
def home():
    rWine = Wine.getRandomSample()
    return render_template(
		"home.html",
		title = "Home",
		subtext = " - Vintage",
		carousel=[
			{"ctitle":"Sparkling", "cimg":"carouselBG1.jpg", "csubtext":"Celebrate",
			"title":"Sparkling wines", "subtext":" - To lighten your celebrations", "category":"Sparkling"},
			
			{"ctitle":"Dessert", "cimg":"carouselBG2.jpg", "csubtext":"Find the most suitable",
			 "title":"Dessert wines", "subtext":" - To finish perfectly", "category":"Dessert"},
			
			{"ctitle":"Vintage", "cimg":"carouselBG3.jpg", "csubtext":"Choose the finest",
			"title":"Vintage wines", "subtext":" - Bottled before year 2000", "category":""}],
        rWine=rWine)
