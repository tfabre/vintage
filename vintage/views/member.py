from vintage.app import app
from vintage.models import User
from flask import render_template
from flask import render_template, url_for, redirect, request
from flask.ext.login import current_user

@app.route("/member/<username>")
def member(username=None):
	if (username == current_user.get_id()):
		user = User.query.get(username)
		baskets = user.getBaskets()
		return render_template("member.html", username=username, user=user, baskets=baskets)
	else:
		return redirect(url_for("home"))
