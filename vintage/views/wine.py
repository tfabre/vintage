from vintage.app import app
from flask import render_template
from flask import request
from vintage.models import Wine
import collections

@app.route("/wine/<int:id>")
def wine(id=0):
   bottle = Wine.getWine(id)
   dictinfos = {"Country":bottle.country, "Quantity":str(bottle.quantity)+" mL", "Region":bottle.region, "Varietal":bottle.varietal}
   dicthead = {"Name":bottle.name, "Vintage":bottle.vintage}
   return render_template(
		"wine.html",
		bottle = bottle,
		infos = collections.OrderedDict(sorted(dictinfos.items())),
		head = collections.OrderedDict(sorted(dicthead.items())))

