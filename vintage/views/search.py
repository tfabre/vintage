from vintage.app import app
from flask import render_template
from flask import request
from vintage.models import Wine

@app.route("/advSearch", methods=["POST", "GET"])
def advSearch():
	nbPerPage = 25
	if request.method == "POST":
		if "next" in request.form:
			page = int(request.form["page"])+1
		elif "prev" in request.form:
			page = int(request.form["page"])-1
		else:
			page = int(request.form["page"])
		if "pagemax" in request.form:
			pagemax = request.form["pagemax"]
		title = request.form["title"]
		name = request.form["name"]
		category = request.form["category"]
		country = capitalize(request.form["country"])
		quantity = request.form["quantity"]
		if quantity != 'None' and quantity != '':
			quantity = int(quantity.split()[0])
		else:
			quantity = None
		region = capitalize(request.form["region"])
		varietal = capitalize(request.form["varietal"])
		vintage = request.form["year"]
		if vintage != 'None' and vintage != '':
			vintage = int(vintage)
		else:
			vintage = None
		if request.form["title"] == "Advanced search submitted":
			sample = Wine.getSample(size=nbPerPage, name=name, category=category, country=country, quantity=quantity, region=region, varietal=varietal, vintage=vintage)
			pagemax = len(sample)-1
			sample = sample[page] if sample != [] else []
			return render_template(
				"search.html",
				title = title,
				subtext = " - Search results",
				sample = sample,
				page = int(page),
				pagemax = int(pagemax),
				name = name,
				category=category,
				country=country,
				quantity=quantity,
				region=region,
				varietal=varietal,
				year=vintage)
		elif request.form["title"] == "Vintage wines":
			sample = Wine.getSample(size=nbPerPage, name=name, category=category, country=country, quantity=quantity, region=region, varietal=varietal, vintage=vintage, vintageInf=2000)[int(page)]
			pagemax = len(sample)-1
			return render_template(
				"search.html",
				title = request.form["title"],
				name = name,
				category=category,
				country=country,
				quantity=quantity,
				region=region,
				varietal=varietal,
				year=vintage,
				sample = sample,
				page=int(page),
				pagemax = pagemax)
		else:
			sample = Wine.getSample(size=nbPerPage, name=name, category=category, country=country, quantity=quantity, region=region, varietal=varietal, vintage=vintage)[page]
			pagemax = len(sample)-1
			return render_template(
				"search.html",
				title = request.form["title"],
				name = name,
				category=category,
				country=country,
				quantity=quantity,
				region=region,
				varietal=varietal,
				year=vintage,
				sample = sample,
				page=int(page),
				pagemax = pagemax)
	else:
		return render_template(
			"search.html",
			title = "Advanced search",
			subtext = " - Find exactly what you are looking for")

def capitalize(s):
   return ' '.join([s[0].upper()+s[1:] for s in s.split()])
