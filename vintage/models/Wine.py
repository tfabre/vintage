from vintage.app import db
from yaml import YAMLObject

class Wine(db.Model):
    __tablename__ = "Wine"
    id       = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(10))
    country  = db.Column(db.String(20))
    image    = db.Column(db.String(50))
    name     = db.Column(db.String(100))
    quantity = db.Column(db.Integer)
    region   = db.Column(db.String(25))
    varietal = db.Column(db.String(20))
    vintage  = db.Column(db.Integer)

    class WineYaml(YAMLObject):
        yaml_tag = u"!Wine"
        
        def __init__(self, id, category, country, image, name, quantity, region,
                     varietal, vintage):
            self.id       = id
            self.category = category
            self.country  = country
            self.image    = image
            self.name     = name
            self.quantity = quantity
            self.region   = region
            self.varietal = varietal
            self.vintage  = vintage

        def __repr__(self):
            return "%s(id=%r, category=%r, country=%r, image=%r, name=%r, quantity=%r, region=%r, varietal=%r, vintage=%r)" % (self.__class__.__name__, self.id, self.category, self.country, self.image, self.name, self.quantity, self.region, self.varietal, self.vintage)
        
        def toOriginalForm(self):
            return Wine(id=self.id, category=self.category,
                        country=self.country, image=self.image, name=self.name,
                        quantity=self.quantity, region=self.region,
                        varietal=self.varietal, vintage=self.vintage)
    
    def toYamlForm(self):
        return self.WineYaml(self.id, self.category, self.country, self.image,
                             self.name, self.quantity, self.region,
                             self.varietal, self.vintage)

    @staticmethod
    def _processQuery(size=10, begin=0, name="", category="", country="",
                      quantity=None, region="", varietal="", vintage=None,
                      vintageInf=None):
        request = Wine.query.filter(Wine.id > begin)
        if name != "":
            request = request.filter(Wine.name.like("%"+name+"%"))
        if category != "":
            request = request.filter(Wine.category == category)
        if country != "":
            request = request.filter(Wine.country == country)
        if quantity != None:
            request = request.filter(Wine.quantity == quantity)
        if region != "":
            request = request.filter(Wine.region == region)
        if varietal != "":
            request = request.filter(Wine.varietal == varietal)
        if vintage != None:
            request = request.filter(Wine.vintage == vintage)
        elif vintageInf != None:
            request = request.filter(Wine.vintage < vintageInf)
        return request

    @staticmethod
    def getPageNumber(size=10, begin=0, name="", category="", country="",
                      quantity=None, region="", varietal="", vintage=None,
                      vintageInf=None):
        request = Wine._processQuery(size=size, begin=begin, name=name,
                                     category=category, country=country,
                                     quantity=quantity, region=region,
                                     varietal=varietal, vintage=vintage,
                                     vintageInf=vintageInf)
        tt = len(request.all())
        return  tt // size + 1 if tt % size > 0 else tt // size

    @staticmethod
    def getSample(size=10, name="", category="", country="",
                  quantity=None, region="", varietal="", vintage=None,
                  vintageInf=None):
        samp = Wine._processQuery(size=size, name=name,
                                  category=category, country=country,
                                  quantity=quantity, region=region,
                                  varietal=varietal, vintage=vintage,
                                  vintageInf=vintageInf).all()

        ret = []
        i = 0
        while i*size < len(samp):
            ret.append(samp[i*size:(i+1)*size])
            i += 1
        return ret

    @staticmethod
    def getWine(id):
        return Wine.query.get_or_404(id)

    @staticmethod
    def getRandomSample(size=3):
        import random
        tt = len(Wine.query.all()) +1
        return [Wine.query.get_or_404(random.randint(1, tt)) for i in range(3)]
