from vintage.app import db
from vintage.models import Wine
from yaml import YAMLObject

class Basket(db.Model):
    __tablename__ = "Basket"
    userId = db.Column(db.String(50), db.ForeignKey("User.username"),
                       primary_key=True)
    wineId = db.Column(db.Integer, db.ForeignKey("Wine.id"), primary_key=True)
    date   = db.Column(db.String(20), primary_key=True)
    wine   = db.relationship("Wine")

    def __init__(self, userId, wineId, date):
        self.userId = userId
        self.wineId = wineId
        self.date   = date
        self.wine   = Wine.query.get(self.userId)

    class BasketYaml(YAMLObject):
        yaml_tag = u"!Basket"

        def __init__(self, userId, wineId, date):
            self.userId = userId
            self.wineId = wineId
            self.date   = date

        def __repr__(self):
            return "%s(userId=%r, wineId=%r, date=%r)" % (
                self.__class__.__name__, self.userId, self.wineId, self.date)
            
        def toOriginalForm(self):
            return Basket(self.userId, self.wineId, self.date)

    def toYamlForm(self):
        return self.BasketYaml(self.userId, self.wineId, self.date)

