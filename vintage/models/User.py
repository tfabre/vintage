from vintage.app import db, login_manager
from vintage.models import Basket
from flask.ext.login import UserMixin
from yaml import YAMLObject
from collections import OrderedDict

class User(db.Model, UserMixin):
    __tablename__ = "User"
    username = db.Column(db.String(50), primary_key=True)
    status   = db.Column(db.String(10))
    password = db.Column(db.String(64))
    baskets  = db.relationship("Basket")

    class UserYaml(YAMLObject):
        yaml_tag = u"!User"

        def __init__(self, username, status, password):
            self.username = username
            self.status   = status
            self.password = password
        
        def __repr__(self):
            return "%s(username=%r, status=%r password=%r)" % (self.__class__.__name__, self.username, self.password)
        
        def toOriginalForm(self):
            return User(username=self.username, status=self.status,
                        password=self.password)

    def toYamlForm(self):
        return self.UserYaml(username=self.username, status=self.status,
                             password=self.password)
            
    def get_id(self):
        return self.username

    def getBaskets(self):
        ret = dict()
        for w in self.baskets:
            ret[w.date] = []
        for w in self.baskets:
            ret[w.date].append(w.wine)

        return OrderedDict(sorted(ret.items()))

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)
