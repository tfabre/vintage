import os.path
from flask import Flask
from flask.ext.script import Manager
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager

def mkpath(path):
    return os.path.normpath(os.path.join(os.path.dirname(__file__), path))

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///"+mkpath("../vintage.db")

manager = Manager(app)

db = SQLAlchemy(app)

Bootstrap(app)

app.config['WTF_CSRF_ENABLED'] = True
app.config['SECRET_KEY'] = '1e09f807-650c-4990-97ca-28d470fb2a31'
app.config['UPLOAD_FOLDER'] = os.path.normpath(os.path.join(os.path.dirname(__file__), 'static/img/wine/images'))
login_manager = LoginManager(app)
login_manager.login_view = "login"

from vintage.models import Wine
app.jinja_env.globals.update(getPageNumber=Wine.getPageNumber)
