from vintage.app import manager, db
from vintage.models import Wine, User, Basket
import yaml

@manager.command
def loaddb(filename):
    '''Creates the tables and populates them with data.'''
    db.create_all()

    data = yaml.load(open(filename))

    for d in data:
        db.session.add(d.toOriginalForm())

    db.session.commit()

@manager.command
def exportdb(filename):
    '''Exports the database to a yaml file.'''

    wines = Wine.query.all()
    users = User.query.all()
    baskets = Basket.query.all()

    yaml.dump([w.toYamlForm() for w in wines], open(filename, 'w'),
              default_flow_style=False)
    file = open(filename, 'a')
    yaml.dump([u.toYamlForm() for u in users], file, default_flow_style=False)
    yaml.dump([b.toYamlForm() for b in baskets], file, default_flow_style=False)

@manager.command
def newuser(username, password):
	'''Adds a new user (arguments are username and password).'''
	from hashlib import sha256
	m = sha256()
	m.update(password.encode())
	user = User.query.get(username)
	if not user:
		u = User(username=username, status="member",
                         password=m.hexdigest())
		db.session.add(u)
		db.session.commit()
		return u
	else:
		return 1
