VINTAGE
===

Vintage is a small wine consultation website made with Python Flask framework.

Installation
---
First install pip (*the python package manager*).

Then use this command :
``` shell
pip install -r requirements.txt
```

To continue, load the database. First, you should install sqlite3. Then use the manage command loaddb with data.yml, like this:
``` shell
./manage.py loaddb vintage/data.yml
```

Note
---
If you modify the database, export it with the manage command **exportdb**, like this:
``` shell
./manage.py exportdb vintage/data.yml
```